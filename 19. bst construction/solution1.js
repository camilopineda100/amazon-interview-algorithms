// Do not edit the class below except for
// the insert, contains, and remove methods.
// Feel free to add new properties and methods
// to the class.
class BST {
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }

  insert(value) {
    // Write your code here.
    // Do not edit the return statement of this method.
		var parentNode = this
		var nextNode = this
		while(nextNode) {
			var parentNode = nextNode
			
			if(value < parentNode.value) {
				nextNode = parentNode.left
			} else {
				nextNode= parentNode.right
			} 
		}
		
		if(value < parentNode.value) {
			parentNode.left = { 
				value,
				left: null,
				right: null 
			}
		} else {
			parentNode.right = { 
				value,
				left: null,
				right: null 
			}
		}
			
    return this;
  }

  contains(value) {
    // Write your code here.
		var currentNode = this
		while(currentNode) {
			if(value < currentNode.value ) {
				currentNode = currentNode.left
			} else if(value > currentNode.value) {
				currentNode = currentNode.right
			} else {
				return true
			}
		}
		
		return false
  }
	
	replaceNode(parent, node) {
		// case1: when node to replace has node childs
		if( !node.left && !node.right ) {
			if(parent) {
				if(node.value < parent.value) {
					parent.left = null
				} else {
					parent.right = null
				}
			}
			return // stop flow
		}
		
		// case2: when node has not a best candidate to the right
		if(!node.right) {
			if(parent) {
				if(node.value < parent.value) {
					parent.left = node.left
				} else {
					parent.right = node.left
				}
			}
			return // stop flow
		}
		
		// case3: node has candidate to replace at right
		// the best candidate is the smallest of the largest
		var candidateParent = node
		var currentNode = node.right
		
		while(currentNode.left) {
			candidateParent = currentNode
			currentNode = currentNode.left
		}
		
		this.replaceNode(candidateParent, currentNode)
		
		currentNode.left = node.left
		currentNode.right = node.right
		
		if(parent) {	
			if(node.value < parent.value) {
				parent.left = currentNode
			} else {
				parent.right = currentNode
			}
		} else {
			this.value = currentNode.value
			this.left = currentNode.left
			this.right = currentNode.right
		}
	}
	
  remove(value) {
    // Write your code here.
    // Do not edit the return statement of this method.
		var parentNode = null
		var currentNode = this
		var nodeToRemove = null
		
		while(currentNode && !nodeToRemove) {
			
			if(value < currentNode.value) {
				parentNode = currentNode
				currentNode = currentNode.left
			} 
			else if(value > currentNode.value) {
				parentNode = currentNode
				currentNode = currentNode.right
			} else {
				nodeToRemove = currentNode
			}
		}
		
		if(nodeToRemove) {
			this.replaceNode(parentNode, nodeToRemove)
		}
		
    return this;
  }
}

// Do not edit the line below.
exports.BST = BST;
