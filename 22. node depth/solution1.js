function nodeDepths(root) {
  // Write your code here.
	// queue element: [node, deep]
	var queue = [[root, 0]]
	var depthSum = 0
	
	while(queue.length) {
		var [node, depth] = queue.shift()
		depthSum+=depth
		
		if(node.left) {
			queue.push([node.left, depth + 1])
		}
		
		if(node.right) {
			queue.push([node.right, depth + 1])
		}
	}
	
	return depthSum
}

// This is the class of the input binary tree.
class BinaryTree {
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

// Do not edit the line below.
exports.nodeDepths = nodeDepths;
