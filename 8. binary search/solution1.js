function binarySearch(array, target) {
  // Write your code here.
	return doBinarySearch(array, target, 0)
}

function doBinarySearch(array, target, initialIndex) {
	var len = array.length
		
	if(len == 1 && target == array [0]) {
		return initialIndex
	} 
	else if (len == 1){
		return -1
	}
	
	var middle = parseInt(len/2) - 1
	
 if(target > array[middle]) {
		initialIndex = initialIndex + middle + 1
		var subArray = array.slice(middle + 1)
		return doBinarySearch(subArray, target, initialIndex)
	} else {
		var subArray = array.slice(0, middle + 1)
		return doBinarySearch(subArray, target, initialIndex)
	}
}

// Do not edit the line below.
exports.binarySearch = binarySearch;
