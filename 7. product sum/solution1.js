// Tip: You can use the Array.isArray function to check whether an item
// is a list or an integer.
function productSum(array) {
  // Write your code here.
	return doProductSum(array, 1)
}

function doProductSum(array, multiplier) {
	var len = array.length
	var sum = 0
	
	for(var i = 0; i < len; i++) {
		var currentElement = array[i]
		
		if(Array.isArray(array[i])) {
			sum = sum + multiplier * doProductSum(array[i],  multiplier + 1)
		} else {
			sum = sum + multiplier * array[i]
		}
		
	}
	
	return sum
}
// Do not edit the line below.
exports.productSum = productSum;
