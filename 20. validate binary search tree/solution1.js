// This is an input class. Do not edit.
class BST {
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

function validateBst(tree) {
  // Write your code here.
	var stack = [[tree, -Infinity, Infinity]]
	
	while(stack.length) {
		var [node, min, max] = stack.shift()
		
		if(node.value < max && node.value >= min ) {
			if(node.right) {
				stack.unshift([node.right, node.value, max])
			}
			if(node.left) {
				stack.unshift([node.left, min, node.value])
			} 
		} else {
			return false
		}	
	}
	
	return true
}

// Do not edit the line below.
exports.BST = BST;
exports.validateBst = validateBst;
