/*
category arrays - medium

<div class="pRAH82oBuvs5qXXJxJWmS"><p>
  Write a function that takes in an array of strings and groups anagrams together.
</p>
<p>
  Anagrams are strings made up of exactly the same letters, where order doesn't
  matter. For example, <span>"cinema"</span> and <span>"iceman"</span> are
  anagrams; similarly, <span>"foo"</span> and <span>"ofo"</span> are anagrams.
</p>
<p>
  Your function should return a list of anagram groups in no particular order.
</p>
<h3>Sample Input</h3>
<pre><span class="CodeEditor-promptParameter">words</span> = ["yo", "act", "flop", "tac", "cat", "oy", "olfp"]
</pre>
<h3>Sample Output</h3>
<pre>[["yo", "oy"], ["flop", "olfp"], ["act", "tac", "cat"]]
</pre>
</div>

*/

/**
 * 
 ---------- Test Case 1 ----------
[ 'yo', 'act', 'flop', 'tac', 'cat', 'oy', 'olfp' ]

---------- Test Case 2 ----------
[]

---------- Test Case 3 ----------
[ 'test' ]

---------- Test Case 4 ----------
[ 'abc', 'dabd', 'bca', 'cab', 'ddba' ]

---------- Test Case 5 ----------
[ 'abc', 'cba', 'bca' ]

---------- Test Case 6 ----------
[ 'zxc', 'asd', 'weq', 'sda', 'qwe', 'xcz' ]

---------- Test Case 7 ----------
[ 'cinema', 'a', 'flop', 'iceman', 'meacyne', 'lofp', 'olfp' ]

---------- Test Case 8 ----------
[ 'abc', 'abe', 'abf', 'abg' ]
 */