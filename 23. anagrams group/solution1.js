function groupAnagrams(words) {
  // Write your code here.
	
	var anagramGroupsMap = new Map()
	var anagramGroups = []
	for( var i = 0; i < words.length; i++) {
		insertInAnagramGroup(anagramGroupsMap, anagramGroups, words[i])
	}
	
	return anagramGroups
}


function insertInAnagramGroup(anagramGroupsMap, anagramGroups, word) {
	var anagramFound = false
	anagramGroupsMap.forEach( (value, key) => {
		if(areAnagrams(key, word)) {
			value.push(word)
			anagramFound = true
		}
	})
	
	if(!anagramFound) {	
		var newAnagramGroup = [word]
		anagramGroups.push(newAnagramGroup)
		anagramGroupsMap.set(word, newAnagramGroup)
	}
}



function areAnagrams(word1, word2) {
	var lenW1 = word1.length
	var lenW2 = word2.length
	
	if(lenW1 != lenW2) {
		return false
	}
	
	var word2Copy = word2.slice()
	for(var i = 0; i < lenW1; i++) {
		var char = word1[i]
		var charIdx = word2Copy.indexOf(char)
		
		if(charIdx == -1) return false
		
		word2Copy = word2Copy.replace(char, '')
	}
	
	return true
} 
// Do not edit the line below.
exports.groupAnagrams = groupAnagrams;
