function caesarCipherEncryptor(string, key) {
  // Write your code here.
	
	// limit 97 - 122
	var newString = ''
	for(var char of string) {
		newString = newString + encryptedChar(char, key)
	}
	
	return newString
}

function encryptedChar(char, key) {
	var firstAscii = 97
	var lastAscii = 122
	var difference =  lastAscii - firstAscii
	var charAscii = char.charCodeAt(0)
	var newCharAscii = (charAscii + key - firstAscii + 1)%(difference + 1)
	
	if (newCharAscii > 0) {
		return String.fromCharCode(firstAscii + newCharAscii - 1)
	} else {
		return  String.fromCharCode(lastAscii)
	}
}


// Do not edit the line below.
exports.caesarCipherEncryptor = caesarCipherEncryptor;
