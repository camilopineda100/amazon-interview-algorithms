function isMonotonic(array) {
  // Write your code here.
	var len = array.length
	if(len == 1) {
		return true
	}
	
	var startIncreasing = undefined
	
	for(var i = 1; i < len; i++) {
		if(startIncreasing === undefined) {
			startIncreasing = array[i] > array[i-1] 
				? true 
				: array[i] < array[i-1] 
					? false
					: undefined
		} 
		else if(startIncreasing && array[i] < array[i-1]) return false
		else if(!startIncreasing && array[i] > array[i-1]) return false
	}
	
	return true
}

// Do not edit the line below.
exports.isMonotonic = isMonotonic;
