function findThreeLargestNumbers(array) {
  // Write your code here.
	var len = array.length
	var threeLargest = []
	
	for(var i = 0; i < len; i++) {
		insertIfLargest(array[i], threeLargest)
	}
	
	return threeLargest
}

function insertIfLargest(number, largestArray) {
	var len = largestArray.length
	
	if( len == 0) {
		largestArray.push(number)
		return
	} 
	
	if(len < 3) {
		insertInPos(number, largestArray)
		return
	}
	
	if(number > largestArray[0]) {
		largestArray.shift()
		insertInPos(number, largestArray)
	}
}

function insertInPos(number, array) {
	var len = array.length
	
	for( var i = 0; i < len; i ++) {
		if(number <= array[i]) {
			array.splice(i, 0, number)
			return
		}
	}
	
	array.push(number)
}

// Do not edit the line below.
exports.findThreeLargestNumbers = findThreeLargestNumbers;
