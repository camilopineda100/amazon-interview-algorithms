// Do not edit the class below except
// for the depthFirstSearch method.
// Feel free to add new properties
// and methods to the class.
class Node {
  constructor(name) {
    this.name = name;
    this.children = [];
  }

  addChild(name) {
    this.children.push(new Node(name));
    return this;
  }
	
  depthFirstSearch(array) {
    // Write your code here.
		array.push(this.name)
		var noChildren = this.children.length
		for(var i = 0; i < noChildren; i++) {
			var currentChildren = this.children[i]
			currentChildren.depthFirstSearch(array)
		}
		
		return array
  }
}

// Do not edit the line below.
exports.Node = Node;
