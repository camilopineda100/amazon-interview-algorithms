// This is the class of the input root.
// Do not edit it.
class BinaryTree {
  constructor(value) {
    this.value = value;
    this.left = null;
    this.right = null;
  }
}

function doBranchSums(node, subtotal, branchSums) {
	var nodeIsALeaf = !node.left && !node.right
	
	if(nodeIsALeaf) {
		branchSums.push(subtotal + node.value)
		return // break the flow
	}
	
	if(node.left) {
		doBranchSums(node.left, subtotal + node.value, branchSums)
	}
	
	if(node.right) {
		doBranchSums(node.right, subtotal + node.value, branchSums)
	}
}

function branchSums(root) {
  // Write your code here.
	var sums = []
	doBranchSums(root, 0, sums)
	return sums
}

// Do not edit the lines below.
exports.BinaryTree = BinaryTree;
exports.branchSums = branchSums;
