
function isValidSubsequence(array, sequence) {
  // Write your code here.
	
	var arrayIndex = 0
	
	for(var i = 0; i < sequence.length; i++) {
		while(array[arrayIndex] != sequence[i] && arrayIndex < array.length) {
			arrayIndex++
		}
		
		if(array[arrayIndex] != sequence[i]) {
			return false
		}
		
		arrayIndex++
	}
	
	return true
}

// Do not edit the line below.
exports.isValidSubsequence = isValidSubsequence;