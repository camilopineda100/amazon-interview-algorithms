function findClosestValueInBst(tree, target) {
  // Write your code here.
	var currentNode = undefined
	var nextNode = tree
	var closestValue = tree.value
	
	while(nextNode) {
		currentNode = nextNode
		
		var diff1 = target - closestValue
		var diff2 = target - currentNode.value
		
		if(Math.abs(diff1) > Math.abs(diff2)) {
			closestValue = currentNode.value
		}
		
		if(target < currentNode.value) {
			nextNode = currentNode.left
		}
		else if(target > currentNode.value) {
			nextNode = currentNode.right
		}
		else { // current node is the target, break loop
			nextNode = null
		}
	}
	
	return closestValue
}

// Do not edit the line below.
exports.findClosestValueInBst = findClosestValueInBst;
