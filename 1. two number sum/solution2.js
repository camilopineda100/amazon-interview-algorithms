function twoNumberSum(array, targetSum) {
  // Write your code here.
	var arrayLen = array.length
	var discoveredNumbers = {}
	
	for(var i = 0; i < arrayLen; i++) {
		var firstNumber = array[i]
		var secondNumber = targetSum - array[i]
		
		if(secondNumber in discoveredNumbers){
			return [firstNumber, secondNumber]
		} else {
			discoveredNumbers[firstNumber] = true
		}
	}
	
	return []
}

// Do not edit the line below.
exports.twoNumberSum = twoNumberSum;