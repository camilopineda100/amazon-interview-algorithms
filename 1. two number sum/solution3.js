function twoNumberSum(array, targetSum) {
  // Write your code here.
	var leftPointer = 0
	var rigthPointer = array.length - 1
	while (leftPointer < rigthPointer) {
		var leftNumber = array[leftPointer]
		var rigthNumber = array[rigthPointer]
		var sum = leftNumber + rigthNumber
		
		if(sum === targetSum) {
			return [leftNumber, rigthNumber]
		}
		else if (sum > targetSum) {
			rigthPointer--
		} else {
			leftPointer++
		}
	}
	
	return []
}

// Do not edit the line below.
exports.twoNumberSum = twoNumberSum;