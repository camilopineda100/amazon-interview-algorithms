function twoNumberSum(array, targetSum) {
  // Write your code here.
	var arrayLen = array.length
	
	if(arrayLen === 1) {
		return []
	}
	
	var firstNumber = array[0]
	for( var i = 1; i <= arrayLen; i++) {
		var secondNumber = array[i]
		var sum = firstNumber+secondNumber
				
		if(sum === targetSum) {
			return [firstNumber, secondNumber]
		}
	}
	
	// it is out of the for loop, firs iteration failed
	var subArray = array.slice(1)
	console.log
	return twoNumberSum(subArray, targetSum)
}

// Do not edit the line below.
exports.twoNumberSum = twoNumberSum;