function moveElementToEnd(array, toMove) {
  // Write your code here.
	for(var i = 0; i < array.length; i++) {
		if(array[i] != toMove) {
			var diffNum = array.splice(i, 1)
			array.unshift(...diffNum)
		}
	}
	
	return array
}

// Do not edit the line below.
exports.moveElementToEnd = moveElementToEnd;
