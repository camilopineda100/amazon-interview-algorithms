function insertionSort(array) {
  // Write your code here.
	for( var i = 1; i < array.length; i++) {
		var subArraySorted = false
		var newElementPos = i
		
		while(!subArraySorted && newElementPos > 0) {
			if(array[newElementPos] < array[newElementPos - 1]) {
				var temp = array[newElementPos]
				array[newElementPos] = array[newElementPos -1]
				array[newElementPos - 1] = temp
				newElementPos--
			} 
			else {
				subArraySorted = true
			}
		}
	}
	
	return array
}

// Do not edit the line below.
exports.insertionSort = insertionSort;
