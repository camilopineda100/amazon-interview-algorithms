function bubbleSort(array) {
  // Write your code here.
	var len = array.length
	var subArrayLen = len
	
	while(subArrayLen > 1) {
		for(var i = 0; i < subArrayLen - 1; i ++) {
			if(array[i] > array[i+1]) {
				var shorterValue = array[i + 1]
				array[i+1] = array[i]
				array[i] = shorterValue
			}
		}
		subArrayLen--
	}
	
	return array
}

// Do not edit the line below.
exports.bubbleSort = bubbleSort;
