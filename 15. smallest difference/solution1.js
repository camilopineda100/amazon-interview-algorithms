function smallestDifference(arrayOne, arrayTwo) {
  // Write your code here.
	var sort1 = arrayOne.sort((a,b)=>a-b)
	var sort2 = arrayTwo.sort((a,b)=>a-b)
	
	var len1 = sort1.length
	var len2 = sort2.length
	
	var first = sort1[len1-1]
	var second = sort2[len2 -1]
	var si2 = len2-1
	var sdiff = Math.abs(first - second)
	
	for(var i=len1-1; i>=0;i--){
		var sif = false
		var sidiff = Math.abs(sort1[i] - sort2[si2])
		if(sidiff < sdiff){
			sdiff=sidiff
			first=sort1[i]
			second=sort2[si2]
		}
		
		while(!sif && si2 > 0) {
			si2--
			var diff = Math.abs(sort1[i]-sort2[si2])
			
			if(diff < sidiff) {
				sidiff=diff
			 if( diff <= sdiff) {
				sdiff = diff
				first = sort1[i]
				second = sort2[si2]
			 } 
			}
			else {
				si2++
				sif=true
			}
		}
	}
	
	return[first,second]
}

// Do not edit the line below.
exports.smallestDifference = smallestDifference;
