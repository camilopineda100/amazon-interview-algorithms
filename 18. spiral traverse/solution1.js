function spiralTraverse(array) {
  // Write your code here.
	
	var xLen = array[0].length
	var yLen = array.length
	
	var left = 0
	var right = xLen - 1
	var up = 0
	var down = yLen - 1
	
	var restElements = xLen * yLen
	
	var newArray = []
	while(restElements > 0) {
		
		if(restElements > 0) {
			// left to right
			for(var i = left; i <= right; i++) {
				newArray.push(array[up][i])
				restElements--
			}
			up++
		}
		
		if(restElements > 0) {
			// up to down
			for(var i = up; i <= down; i++) {
				newArray.push(array[i][right])
				restElements--
			}
			right--
		}
		
		if(restElements > 0) {
			// right to left
			for(var i = right; i >= left; i--) {
				newArray.push(array[down][i])
				restElements--
			}
			down--
		}
		
		if(restElements > 0) {
			// down to up
			for(var i = down; i >= up; i--) {
				newArray.push(array[i][left])
				restElements--
			}
			left++
		}
	}
	
	return newArray
}

// Do not edit the line below.
exports.spiralTraverse = spiralTraverse;
