function isPalindrome(string) {
  // Write your code here.
	var left = 0
	var right = string.length - 1
	
	while(left <= right) {
		if(string[left] !== string[right]) {
			return false
		} 
		
		left++
		right--
	}
	
	return true
}

// Do not edit the line below.
exports.isPalindrome = isPalindrome;
