function threeNumberSum(array, targetSum) {
  // Write your code here.
	array.sort((a,b) => a - b)
	var arrayLen = array.length
	var tripletsArray = []
	for( var i = 0; i < arrayLen - 2; i++) {
		var left = i + 1
		var right = arrayLen - 1
		
		while(left < right) {
			var sum = array[i] + array[left] + array[right]
	
			if (sum === targetSum) {
				tripletsArray.push([array[i], array[left], array[right]])
				left++
				right--
			} 
			else if(sum < targetSum) {
				left++
			} 
			else {
				right--
			}
		}
	}
	
	return tripletsArray
}

// Do not edit the line below.
exports.threeNumberSum = threeNumberSum;
