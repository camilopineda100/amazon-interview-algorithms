function threeNumberSum(array, targetSum) {
  // Write your code here.
	var arrayLen = array.length
	var tripletsArray = []
	array.sort((a,b) => a - b)
	
	for(var f = 0; f < arrayLen - 2; f++) {
		var firstNumber = array[f]
		
		for(var s = f + 1; s < arrayLen - 1; s++) {
			var secondNumber = array[s]
			
			for(var t = s + 1; t < arrayLen; t++) {
				var thirdNumber = array[t]
				var sum = firstNumber + secondNumber + thirdNumber
				
				if(sum === targetSum) {
					tripletsArray.push([firstNumber, secondNumber, thirdNumber])
				}
			}
		}
	}
	
	return tripletsArray
}

// Do not edit the line below.
exports.threeNumberSum = threeNumberSum;
