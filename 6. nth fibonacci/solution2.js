function getNthFib(n) {
  // Write your code here.
	if(n == 1) return 0
	if(n == 2) return 1
	
	var restN = n - 2
	var firstFib = 0
	var secondFib = 1
	
	for(restN; restN >= 1; restN-- ) {
		if(restN == 1) {
			return firstFib + secondFib
		} else {
			var currentFib = firstFib + secondFib
			firstFib = secondFib
			secondFib = currentFib
		}
	}
}

// Do not edit the line below.
exports.getNthFib = getNthFib;
