function getNthFib(n) {
  // Write your code here.
	if(n == 1) return 0
	if(n == 2) return 1
	
	return getFib(0, 1, n - 2)
}

function getFib(first, second, restN) {
	if(restN == 1) {
		return first + second
	} else {
		return getFib(second, first + second, restN - 1 )
	}
}

// Do not edit the line below.
exports.getNthFib = getNthFib;
