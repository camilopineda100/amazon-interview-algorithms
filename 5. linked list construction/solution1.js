// This is an input class. Do not edit.
class Node {
  constructor(value) {
    this.value = value;
    this.prev = null;
    this.next = null;
  }
}

// Feel free to add new properties and methods to the class.
class DoublyLinkedList {
  constructor() {
    this.head = null;
    this.tail = null;
  }

  setHead(node) {
    // Write your code here.
		if(this.head && node != this.head) {
			// in case the node is in the linked list
			this.remove(node)
			
			node.next = this.head
			this.head.prev = node
			this.head = node
		} 
		else if(!this.head) {
			this.head = node
			this.tail = node
		}
  }

  setTail(node) {
    // Write your code here.
		if(this.tail && node != this.tail) {
			// remove node first in case is in the linked list
			this.remove(node)
			
			node.prev = this.tail
			this.tail.next = node
			this.tail = node
		} 
		else if(!this.tail) {
			this.tail = node
			this.head = node
		}
  }

  insertBefore(node, nodeToInsert) {
    // Write your code here.
		var currentNode = this.head
		
		// move from head to tail
		while(currentNode) {
			if(currentNode == node) { // validate for node and not for value ??
				// in case the node is in the linked list
				this.remove(nodeToInsert)
				
				var prevNode = currentNode.prev
				
				if(prevNode) {
					prevNode.next = nodeToInsert
					nodeToInsert.prev = prevNode
					nodeToInsert.next = currentNode
					currentNode.prev = nodeToInsert
				} else {
					nodeToInsert.next = currentNode
					currentNode.prev = nodeToInsert
					this.head = nodeToInsert
				}
				
				return // node found and node inserted before
			} else {
				currentNode = currentNode.next
			}
		}
  }

  insertAfter(node, nodeToInsert) {
    // Write your code here.
		var currentNode = this.head
		
		// traverse list from head to tail
		while(currentNode) {
			if(currentNode == node) {
				// in case the node is in the linked list
				this.remove(nodeToInsert)
				
				var nextNode = currentNode.next
				
				if (nextNode) {
					currentNode.next = nodeToInsert
					nodeToInsert.prev = currentNode
					nodeToInsert.next = nextNode
					nextNode.prev = nodeToInsert
				} else { // current node is the tail
					nodeToInsert.prev = currentNode
					currentNode.next = nodeToInsert
					this.tail = nodeToInsert
				}
				
				return // node found and inserted after
			} else {
				currentNode = currentNode.next
			}
		}
  }

  insertAtPosition(position, nodeToInsert) {
    // Write your code here.
		var currentPos = 1
		var currentNode = this.head
		
		if(position == 1) {
			this.setHead(nodeToInsert)
		}
		
		while(currentNode) {
			if(currentPos == position) {
				this.insertBefore(currentNode, nodeToInsert)
				return
			}
			currentNode = currentNode.next
			currentPos++
		}
		
		if(currentPos == position) {
			this.setTail(nodeToInsert)
		}
  }

  removeNodesWithValue(value) {
    // Write your code here.
		var currentNode = this.head
		
		while(currentNode) {
			if(currentNode.value == value) {
				var nextNode = currentNode.next
				this.remove(currentNode)
				currentNode = nextNode
			} else {
				currentNode = currentNode.next
			}
		}
  }

  remove(node) {
    // Write your code here.
		var currentNode = this.head
		
		while(currentNode) {
			if(currentNode == node) {
				var prevNode = currentNode.prev
				var nextNode = currentNode.next
				
				if(prevNode && nextNode) {
					prevNode.next = nextNode
					nextNode.prev = prevNode
				}
				else if(prevNode) {
					prevNode.next = null
					this.tail = prevNode
				} 
				else if (nextNode) {
					nextNode.prev = null
					this.head = nextNode
				} else {
					this.head = null
					this.tail = null
				}
				
				node.prev = null
				node.next = null
				return // node remove	
			}
			
			currentNode = currentNode.next		
		}
  }

  containsNodeWithValue(value) {
    // Write your code here.
		var currentNode = this.head
		
		while(currentNode) {
			if(currentNode.value == value) {
				return true
			}
			
			currentNode = currentNode.next
		}
		
		return false
  }
}

// Do not edit the line below.
exports.Node = Node;
exports.DoublyLinkedList = DoublyLinkedList;
